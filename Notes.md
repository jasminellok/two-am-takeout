# This file is for taking notes as you go.

# Ran go run ./server, test results

# Part 1 with failing backend test:
- run tests
=== RUN   TestServerSuite/TestHealthCheck
    2021/03/04 19:25:10 starting server on port 3000
    main_test.go:55: 
                Error Trace:    main_test.go:55
                Error:          "{\"OK\":true}\n" does not contain "\"ok\":true"
                Test:           TestServerSuite/TestHealthCheck
- test shows that we got OK: true and not what was expected ok:true, so the key is not consistent. 
- found where this response string formatted in handler, updated HealthBody response to `json:"ok"`
- test pass


# Part 2 with passing test but test assumption about what needs to be tested is incorrect
- looking into some goland websocket stuff:
    - syntax stuff: https://tour.golang.org/concurrency/2, https://tour.golang.org/flowcontrol/12
    - on handlers: https://gobyexample.com/http-servers
    - understanding go keyword: https://stackoverflow.com/questions/26006856/why-use-the-go-keyword-when-calling-a-function
    - info on ticker: https://gobyexample.com/tickers
    - info on WriteMessage, websockets: https://stackoverflow.com/questions/65616177/how-do-writemessage-and-readmessage-in-gorilla-websocket-work

- go through sockets frontend and backend to figure out interaction
    - main start server create+run new hub, establish routes and connects to handler
    - hub handles messages and broadcast messages, delete/close clients (readpump & writepump interacts with hub)
    - handler returns sockets.ServeWs(h, w, r), and ServeWs handles websocket requests. If no error register client (true), start readpump writepump thread
    -  writepump: pumps messages from the hub to the websocket connection
        - case message: if there is issue with message, close channel / return, else add queued chat messages to the current websocket message.
        - case ticker (repeate at intervals): set deadline for write calls
            - setting err and if it doesnt == to nil return
            - if nothing is wrong, expect nil, else return 
    - readpump: pumps messages from the websocket connection to the hub
        - more notes in client.go
- how the frontend expects to communicate:
    - Notes of test in test file
    **Still trying to understand interaction, covered up to readpump and moving on to hub.go**
- what does the backend interact with frontend:

